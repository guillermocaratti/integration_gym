package ar.edu.uade.inte.gym.view;

import java.io.OutputStream;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;


import ar.edu.uade.inte.gym.dao.ExportacionExcelController;
import ar.edu.uade.inte.gym.exception.Invalid;

@Named
@SessionScoped
public class ExportacionMB implements Serializable{
	
	private static final long serialVersionUID = -8888780667484643240L;
	
	@Inject
	private ExportacionExcelController exportacion;
	
	public void exportar() {
		
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();
	    ec.responseReset();
	    
	    ec.setResponseHeader("Content-Type", "application/vnd.ms-excel");
	    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "Empleados" + "\"");
	    try (OutputStream output = ec.getResponseOutputStream()) {
		try {
			exportacion.liquidar(output);
		} catch (Invalid e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    } catch (Exception e) {
	        System.out.println("Error al intentar crear el excel " + e.getMessage());
	    }
		fc.responseComplete();
	}
	

}
