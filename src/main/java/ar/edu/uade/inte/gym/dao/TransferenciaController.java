package ar.edu.uade.inte.gym.dao;

import java.io.Serializable;

import javax.ejb.Stateless;


import ar.edu.uade.inte.gym.bean.Transferencia;
@Stateless
public class TransferenciaController extends EntityController<Transferencia> implements Serializable{
	private static final long serialVersionUID = 1635523948951895899L;

	@Override
	public Class<Transferencia> getEntityClass() {
		// TODO Auto-generated method stub
		return Transferencia.class;
	}

}
