package ar.edu.uade.inte.gym.bean;

import java.io.Serializable;

import javax.persistence.Entity;
@Entity
public class Tarjeta extends TipoDePago implements Serializable{
	private static final long serialVersionUID = 1635523948951895899L;
	
	private String numero;
	private long codseg;
	
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public long getCodseg() {
		return codseg;
	}
	public void setCodseg(long codseg) {
		this.codseg = codseg;
	}
	
	
	
	
}
