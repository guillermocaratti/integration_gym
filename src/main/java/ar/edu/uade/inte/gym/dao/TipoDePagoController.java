package ar.edu.uade.inte.gym.dao;


import javax.ejb.Stateless;

import ar.edu.uade.inte.gym.bean.TipoDePago;
@Stateless
public class TipoDePagoController extends EntityController<TipoDePago>{
	
	private static final long serialVersionUID = 1635523948951895899L;

	@Override
	public Class<TipoDePago> getEntityClass() {
		// TODO Auto-generated method stub
		return TipoDePago.class;
	}

}
