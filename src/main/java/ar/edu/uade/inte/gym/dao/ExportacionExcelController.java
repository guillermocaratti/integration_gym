package ar.edu.uade.inte.gym.dao;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.swing.JFileChooser;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import ar.edu.uade.inte.gym.bean.Administrativo;
import ar.edu.uade.inte.gym.bean.Profesor;
import ar.edu.uade.inte.gym.exception.Invalid;

@Stateless
public class ExportacionExcelController {
	
	private static final long serialVersionUID = 6266719709947160418L;
	
	@Inject
	private ProfesorController profesorController;
	
	@Inject
	private AdministrativoController administrativoController;
	
	@Inject
	private ReciboDeSueldoController reciboController;
	
	public void liquidar(OutputStream outputStream) throws Invalid {
	
		List<Administrativo> administrativos = new ArrayList<Administrativo>();
		List<Profesor> profesores = new ArrayList<Profesor>();
		//JFileChooser seleccionar = new JFileChooser();
        File archivo;
        
        profesores.addAll(profesorController.getAll());
        administrativos.addAll(administrativoController.getAll());
        
        System.out.println("Cantidad Profesores: " + profesores.size());
        System.out.println("Cantidad Administrativos: " + administrativos.size());
        
        
        /*if (seleccionar.showDialog(null, "Exportar a Excel") == JFileChooser.APPROVE_OPTION){
            archivo = seleccionar.getSelectedFile();*/
            //int cantFila = tbNivelArchivo.getRowCount();
            //int cantColumna = tbNivelArchivo.getColumnCount();
            
            String[] headers1 = new String[]{
                    "NroEmpleado",
                    "DNI",
                    "Dias"
            };

            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet hoja = ((org.apache.poi.ss.usermodel.Workbook) wb).createSheet("Administrativos");
            CellStyle style = wb.createCellStyle();
            try {
            	
            	Row fila = hoja.createRow(0);
            	for(int i = 0; i < headers1.length; i++) {

                    Cell celda = fila.createCell(i);
                    celda.setCellStyle(style); 
                    celda.setCellValue(headers1[i]);
                }
                  
            	
            	int i=1;
            	
               for(Administrativo adm : administrativos) {
            	   Row fila1 = hoja.createRow(i);
            	   Cell celda = fila1.createCell(0);
            	   celda.setCellValue(adm.getNroEmpleado());
            	   Cell celda1 =fila1.createCell(1);
            	   celda1.setCellValue(adm.getDni());
            	   Cell celda2 = fila1.createCell(2);
            	   celda2.setCellValue(30);
            	   
            	   i++;
            	   
               }
                   
               
               } catch (Exception e) {
                   //JOptionPane.showMessageDialog(null, "Vuelve a intentarlo");
               }
            
            String[] headers2 = new String[]{
                    "NroEmpleado",
                    "DNI",
                    "HS"
            };
        
            Sheet hoja2 = ((org.apache.poi.ss.usermodel.Workbook) wb).createSheet("Profesores");
            //Row fila1 = hoja2.createRow(0);
            
            
            try {
            	///////////////
            	
            	Row fila = hoja2.createRow(0);
            	for(int i = 0; i < headers2.length; i++) {

                    Cell celda = fila.createCell(i);
                    celda.setCellStyle(style); 
                    celda.setCellValue(headers2[i]);
                }
            	
            	
            	int i=1;
            	for(Profesor prof : profesores) {
            	
            		Row fila2 = hoja2.createRow(i);
             	   Cell celda = fila2.createCell(0);
             	   celda.setCellValue(prof.getNroEmpleado());
             	   Cell celda1 =fila2.createCell(1);
             	   celda1.setCellValue(prof.getDni());
             	   Cell celda2 = fila2.createCell(2);
             	   celda2.setCellValue(reciboController.liquidarHsProfesor(prof));
             	   
             	   i++;
             	   
                }
                   //JOptionPane.showMessageDialog(null, "Exportacion exitosa");
            	wb.write(outputStream);
            	outputStream.flush();
                outputStream.close();
                   //Desktop.getDesktop().open(archivo);
               } catch (Exception e) {
                   //JOptionPane.showMessageDialog(null, "Vuelve a intentarlo");
               }
            
            
        }
	}

//}
