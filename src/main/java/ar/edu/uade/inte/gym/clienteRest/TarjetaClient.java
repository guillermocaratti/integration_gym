package ar.edu.uade.inte.gym.clienteRest;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import ar.edu.uade.inte.gym.bean.Factura;
import ar.edu.uade.inte.gym.bean.Socio;
import ar.edu.uade.inte.gym.bean.Tarjeta;
import ar.edu.uade.inte.gym.clienteRest.entidades.ConsumosTarjetaRequest;



@Stateless
public class TarjetaClient {

	private static final String TARJETA_URL = "192.168.xxx.xxx:8090/tarjetas";
	
//	public AltaTarjetaResponse alta(AltaTarjetaRequest altaReq) {
//		Client client = ClientBuilder.newClient();
//		return client.target(TARJETA_URL)
//				.path("consumosEnteros") // le agrega a la URL de arriba el path /alta
//				.request(MediaType.APPLICATION_JSON)  // Le dice que va a pedir un JSON
//				.post(Entity.entity(altaReq, MediaType.APPLICATION_JSON)  //Dice que va a mandar un JSON del altaReq
//						,AltaTarjetaResponse.class); //Dice que lo que te transforme el JSON que devuelve en un AltaReponse
//	}
	
	public void consume(String cbu,ConsumosTarjetaRequest consumosReq) {
		Client client = ClientBuilder.newClient();
		client.target(TARJETA_URL)
				.path(cbu) // le agrega a la URL de arriba el path /consumos
				.request(MediaType.APPLICATION_JSON)  // Le dice que va a pedir un JSON
				.post(Entity.entity(consumosReq, MediaType.APPLICATION_JSON)); //Dice que va a mandar un JSON del altaReq
	}

	
	private void cobrarFactura(Factura factura) {
		Tarjeta tarjeta = (Tarjeta) factura.getSocio().getTipodepago();
		Socio socio = factura.getSocio();
		double monto = factura.getMonto();
		System.out.println("Se le va a cobrar por tarjeta con numero" + 
				tarjeta.getNumero() + " a nombre de " + 
				socio.getNombre() + " un monto de " + 
				monto);
		//TODO
	}
	
	public void cobrarFacturas(List<Factura> facturas) {
		for (Factura factura : facturas) {
			cobrarFactura(factura);
		}
	}
	
	
	
}
