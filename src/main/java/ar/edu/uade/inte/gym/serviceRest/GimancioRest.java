package ar.edu.uade.inte.gym.serviceRest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ar.edu.uade.inte.gym.bean.Gimnasio;
import ar.edu.uade.inte.gym.dao.GimnasioController;

@Path("gimnasio")
public class GimancioRest {
	
	@Inject
	private GimnasioController gimnasioController;

	@GET
	public Response getAll (){
		List<Gimnasio> all = gimnasioController.getAll();
		return Response
                .ok(all)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
