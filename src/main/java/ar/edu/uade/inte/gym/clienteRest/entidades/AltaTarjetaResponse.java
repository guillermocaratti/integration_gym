package ar.edu.uade.inte.gym.clienteRest.entidades;

import java.io.Serializable;

public class AltaTarjetaResponse implements Serializable{

	private static final long serialVersionUID = -6926985660695588534L;

	private Long idEstablecimiento;

	public Long getIdEstablecimiento() {
		return idEstablecimiento;
	}

	public void setIdEstablecimiento(Long idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

}

