package ar.edu.uade.inte.gym.view;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import ar.edu.uade.inte.gym.bean.Socio;
import ar.edu.uade.inte.gym.bean.Tarjeta;
import ar.edu.uade.inte.gym.dao.SocioController;
import ar.edu.uade.inte.gym.dao.TarjetaController;

@Named
@SessionScoped
public class MedioPagoMB implements Serializable{

	private static final long serialVersionUID = -8888780667484643240L;

	private Socio socio = new Socio();
	
	@Inject
	private SocioController socioController;
	
	@Inject
	private TarjetaController tarjetaController; 
	
	private Tarjeta tarjeta = new Tarjeta();


	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (params.containsKey("id")) {
			this.socio = socioController.byId(Integer.valueOf(params.get("id")));
		} else {
			System.out.println("No se encontro el socio");
		}
	}
	
	
	public void asociarTarjetaSocio(){
		tarjeta.setFecha(new Date());
		
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}


	public Tarjeta getTarjeta() {
		return tarjeta;
	}


	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}


}
