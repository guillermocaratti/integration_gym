package ar.edu.uade.inte.gym.dao;

import javax.ejb.Stateless;

import ar.edu.uade.inte.gym.bean.Gimnasio;

@Stateless
public class GimnasioController  extends EntityController<Gimnasio>{

	private static final long serialVersionUID = -1302041725490866424L;

	@Override
	public Class<Gimnasio> getEntityClass() {
		return Gimnasio.class;
	}


}
