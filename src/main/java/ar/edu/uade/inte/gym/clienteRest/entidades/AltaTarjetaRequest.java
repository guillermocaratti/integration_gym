package ar.edu.uade.inte.gym.clienteRest.entidades;

import java.io.Serializable;

public class AltaTarjetaRequest implements Serializable{
	
	private static final long serialVersionUID = -2857257301070481285L;

	private String cuit;

	private String cbu;
	
	private String razonSocial;
	
	private String domicilio;

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
}