package ar.edu.uade.inte.gym.clienteRest.entidades;


import java.io.Serializable;
import java.util.Date;

public class ConsumosTarjetaRequest implements Serializable{

	private static final long serialVersionUID = 3129953712389848496L;

	private Long idEstablecimiento;
	
	private String nroTarjeta;
    
	private Long codigoSeguridad;
    
	private Date fecha;

	private String descripcion;
    
	private Double monto;
    
	private Integer cantCuotas=1;
    
	private Long interes;

	public Long getIdEstablecimiento() {
		return idEstablecimiento;
	}

	public void setIdEstablecimiento(Long idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public String getNroTarjeta() {
		return nroTarjeta;
	}

	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}

	public Long getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(Long codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Integer getCantCuotas() {
		return cantCuotas;
	}

	public void setCantCuotas(Integer cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	public Long getInteres() {
		return interes;
	}

	public void setInteres(Long interes) {
		this.interes = interes;
	}
    
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}
