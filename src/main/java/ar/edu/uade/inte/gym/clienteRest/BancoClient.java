package ar.edu.uade.inte.gym.clienteRest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import ar.edu.uade.inte.gym.bean.Factura;
import ar.edu.uade.inte.gym.bean.ReciboDeSueldo;
import ar.edu.uade.inte.gym.bean.Socio;
import ar.edu.uade.inte.gym.bean.Transferencia;
import ar.edu.uade.inte.gym.clienteRest.entidades.TransferenciaBancoRequest;
import ar.edu.uade.inte.gym.dao.ReciboDeSueldoController;

@Stateless
public class BancoClient {
	
	@Inject
	ReciboDeSueldoController reciboSueldoctrl;
	
	private static final String TARJETA_URL = "http://192.168.215.34:8080/api";
	
	public void depositarEmpleados(	List<ReciboDeSueldo> recibos){
		for (ReciboDeSueldo reciboDeSueldo : recibos) {
			TransferenciaBancoRequest transferenciaRecibo = new TransferenciaBancoRequest();
			transferenciaRecibo.setOrigen("gimnasioCBU");
			transferenciaRecibo.setDestino(reciboDeSueldo.getEmpleado().getCbu());
			transferenciaRecibo.setMonto(reciboDeSueldo.getMonto());
			transferenciaRecibo.setDescripcion("gimansio");
			movimiento(transferenciaRecibo);
		}
	}
	
	
	public void movimiento(TransferenciaBancoRequest transfBancoReq) {
		Client client = ClientBuilder.newClient();
		client.target(TARJETA_URL)
				.path("transferencia") // le agrega a la URL de arriba el path /alta
				.request(MediaType.APPLICATION_JSON)  // Le dice que va a pedir un JSON
				.post(Entity.entity(transfBancoReq, MediaType.APPLICATION_JSON)); //Dice que lo que te transforme el JSON que devuelve en un AltaReponse
	}


	public void cobrarFacturas(List<Factura> cobrarBanco) {
		for (Factura factura : cobrarBanco) {
			cobrarFatura(factura);
		}
	}


	private void cobrarFatura(Factura factura) {
		Socio socio = factura.getSocio(); 
		Transferencia transferencia = (Transferencia) factura.getSocio().getTipodepago();
		double monto = factura.getMonto();
		System.out.println("Se le va a cobrar por transferencia " + 
				monto + " a " + 
				socio.getNombre() + " con cbu " +
				transferencia.getCbu());
		//TODO
	}
	
	

}
