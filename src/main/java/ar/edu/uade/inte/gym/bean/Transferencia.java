package ar.edu.uade.inte.gym.bean;

import java.io.Serializable;

import javax.persistence.Entity;
@Entity
public class Transferencia extends TipoDePago implements Serializable{

	private static final long serialVersionUID = 5356626734934663660L;
	
	private String cbu;

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	
	
}
