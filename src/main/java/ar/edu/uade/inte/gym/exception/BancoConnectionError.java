package ar.edu.uade.inte.gym.exception;

import javax.validation.ValidationException;

public class BancoConnectionError extends Exception{
	private static final long serialVersionUID = 2260054145693830712L;

	public BancoConnectionError(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BancoConnectionError(String message, Throwable cause) {
		super(message, cause);
	}

	public BancoConnectionError(String message) {
		super(message);
	}
	
	public BancoConnectionError(ValidationException cause) {
		super(cause.getLocalizedMessage(),cause);
	}

	public BancoConnectionError(Throwable cause) {
		super("Error al comunicarse con el banco",cause);
	}

	
}
